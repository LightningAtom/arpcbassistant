﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parser : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Dictionary<string, string> result = new Dictionary<string, string>();
        string designator, partNumber, description;

        TextAsset csvData = Resources.Load<TextAsset>("20181130");
        string[] data = csvData.text.Split(new char[] { '\n' });        

        for (int i = 1; i < data.Length - 1; i++)
        {
            string[] row = data[i].Split(new char[] { ';' });

            if (row[1] != "")
            {
                designator = row[0];
                partNumber = row[1];
                description = row[2];
                try
                {
                    result.Add(designator, partNumber + "\n" + description);
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("An element with Key = " + designator + " already exists.");
                }
            }
        }

        string TEST_KEY = "C45";
        try
        {
            Debug.Log("Value by Key[" + TEST_KEY + "] is [" + result[TEST_KEY] + "]");
        }
        catch (KeyNotFoundException)
        {
            Debug.Log("Key [" + TEST_KEY + "] not found");
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}


}
